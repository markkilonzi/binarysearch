/**
 * Created by marko on 3/21/17.
 */
public class BinarySearch {

    private int binarySearch(int [] array, int searchItem, int low, int high){

     int mid = low + (high - low) / 2;

       if(low <= high){

            if(array[mid] == searchItem){

                return mid;
            }

            else if(searchItem < array[mid]){

                high = mid - 1;
                return binarySearch(array, searchItem, low, high);
            }

            else if(searchItem > array[mid]){

                low = mid + 1;
                return binarySearch(array, searchItem, low, high);
            }

       }
            return -1;
        //return -1 if searchItem is not found
    }

    public static void main(String[] args) {

        int [] sampleArray = {3 , 25, 28, 37, 44, 51, 62, 77, 89, 91, 103};

        int key = 103;

        BinarySearch newBinarySearch = new BinarySearch();
        System.out.println(newBinarySearch.binarySearch(sampleArray, key, 0, sampleArray.length-1));
    }
}
